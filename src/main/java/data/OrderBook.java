package data;

import java.io.Serializable;

public class OrderBook implements Serializable {
    private static final long serialVersionUID = 1L;
    Long timestmp;
    String data;
    public Long getTimestmp() {
        return timestmp;
    }

    public void setTimestmp(Long timestmp) {
        this.timestmp = timestmp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
