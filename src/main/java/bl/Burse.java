package bl;

import com.google.common.base.Joiner;
import db.DatabaseImpl;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;


public class Burse {
    private static final Logger logger = LoggerFactory.getLogger(Burse.class);
    private HttpGet dataRequest;
    private HttpClient httpClient;
    private ResponseHandler<String> responseHandler;
    public Burse(){
        httpClient = HttpClientBuilder.create()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD).build()).build();
        responseHandler =response -> {
            int status = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            if (status>=200 && status<300){
                logger.debug("Packet is successful accepted");
                return entity!=null? EntityUtils.toString(entity):null;
            }else {
                switch (status){
                    default:logger.warn("Should catch errors and raise them");
                }
                throw new ClientProtocolException("Unexpected response status");
            }
        };
    }
    public String getOrderBook(String endPoint) throws IOException{
        dataRequest = new HttpGet(endPoint);
        logger.info("request sent to api: "+ endPoint);
        String respBody = httpClient.execute(dataRequest, responseHandler);
        String [] splitedString = respBody.split(",");
        Long timestmp = Long.parseLong((splitedString[0].split(":")[1].replaceAll("[\" ]", "")));
        DatabaseImpl db = new DatabaseImpl();
        db.saveOrdersToDB(timestmp, Joiner.on("").join( Arrays.copyOfRange(splitedString,1,splitedString.length)));
        return respBody;
    }

    public String getCurrency(String endPoint,String currencyPair) throws IOException{
        dataRequest = new HttpGet(endPoint+currencyPair);
        logger.info("request sent to api: "+ endPoint+currencyPair);
        String respBody = httpClient.execute(dataRequest, responseHandler);
        return respBody;
    }
}
