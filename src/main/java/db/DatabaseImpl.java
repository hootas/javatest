package db;
import data.OrderBook;
import persistence.HibernateUtil;
import org.hibernate.Session;
public  class DatabaseImpl {
    Session session = HibernateUtil.getSessionFactory().openSession();
    public boolean saveOrdersToDB(Long timestmp,String data) {
        try {
            session.beginTransaction();
            OrderBook orderBook = new OrderBook();
            orderBook.setTimestmp(timestmp);
            orderBook.setData(data);
            session.save(orderBook);
            session.getTransaction().commit();
            return true;
        } catch (Exception e){
            return false;
        } finally {
            session.close();
        }
    }
}
