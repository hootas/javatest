import bl.Burse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Scanner;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(String[] args) {
        Burse burse = new Burse();
        logger.info("input \"command\" for get order book");
        logger.info("input pair key for get pairs order book");
        logger.info("press enter for exit");
        try {
            Scanner in = new Scanner(System.in);
            outerloop:
            while (true){
                String line;
                switch ( line =in.nextLine()){
                    case "":
                        break outerloop;
                    case "Command":
                        logger.info(burse.getOrderBook("http://www.bitstamp.net/api/order_book/"));
                        break;
                    default:
                        logger.info(burse.getCurrency("http://www.bitstamp.net/api/v2/order_book/",line));
                }
            }
        }catch (IOException e){
            logger.error(e.getMessage());
        }
    }
}
